       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING-3.
       AUTHOR. ARTHORN.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  REPCOUNT       PIC 9(4).
       01  PRNREPCOUNT    PIC Z,ZZ9.
       01  NUMBEROFTIMES  PIC 9(4) VALUE 1000.

       PROCEDURE DIVISION.
       BEGIN.
           PERFORM VARYING REPCOUNT  FROM 0 BY 50
                    UNTIL  REPCOUNT  = NUMBEROFTIMES 
              MOVE REPCOUNT  TO PRNREPCOUNT 
              DISPLAY "counting " PRNREPCOUNT 
           END-PERFORM
           MOVE REPCOUNT TO PRNREPCOUNT
           DISPLAY "If I have told you once"
           DISPLAY "I've told you " PRNREPCOUNT " times."

           GOBACK 
           .