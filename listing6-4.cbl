       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING-4.
       AUTHOR. ARTHORN.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  Counters.
           05 Hundreds-Count  PIC 99   VALUE ZEROES.
           05 Tens-Count      PIC 99   VALUE ZEROES.
           05 Units-Count     PIC 99   VALUE ZEROES.
       01  Odometer.
           05 Prn-Hundreds    PIC 9.
           05 FILLER         PIC X    VALUE "-".
           05 Prn-Tens        PIC 9.
           05 FILLER         PIC X    VALUE "-".
           05 Prn-Units       PIC 9.
       
       PROCEDURE DIVISION.
       000-BEGIN.
           DISPLAY "Using an out-of-line Perform".
           PERFORM 001-COUNT-MILEAGE THRU 001-EXIT 
              VARYING Hundreds-Count FROM 0 BY 1
                 UNTIL Hundreds-Count > 9
              AFTER Tens-Count  FROM 0 BY 1 UNTIL Tens-Count > 9
              AFTER Units-Count  FROM 0 BY 1 UNTIL Units-Count > 9
           
      *    PERFORM VARYING Hundreds-Count FROM 0 BY 1
      *          UNTIL Hundreds-Count > 9
      *       PERFORM VARYING Tens-Count  FROM 0 BY 1 
      *          UNTIL Tens-Count > 9
      *          PERFORM VARYING Units-Count  FROM 0 BY 1 
      *             UNTIL Units-Count > 9
      *                MOVE Hundreds-Count TO Prn-Hundreds 
      *                MOVE Tens-Count  TO Prn-Tens  
      *                MOVE Units-Count  TO Prn-Units  
      *                DISPLAY "Out - " Odometer 
      *          END-PERFORM
      *       END-PERFORM
      *    END-PERFORM
           GOBACK
       .
       001-COUNT-MILEAGE.
           MOVE Hundreds-Count TO Prn-Hundreds 
           MOVE Tens-Count  TO Prn-Tens  
           MOVE Units-Count  TO Prn-Units  
           DISPLAY "Out - " Odometer 
       .
       001-EXIT.
           EXIT
       .
            